
module.exports = async  (callback) => {
    password = 'foo'
    console.log('started')
    alice = await web3.eth.accounts.create()
    bob = await web3.eth.accounts.create()
    web3.eth.personal.unlockAccount(alice.address, password, 60)
    tx = {from: alice.address, to: bob.address, value: 1000000000000000000, gas: 21000, gasPrice: 10000000000 }
    console.log(tx)
    result = web3.eth.sendTransaction(tx)
    console.log('sending', result)
    console.log('result', await result)
    console.log('done')
    callback() 
}