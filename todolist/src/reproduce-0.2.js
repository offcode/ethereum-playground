// echo foo > Password
// geth --password Password js src/reproduce-0.2.js

password = 'foo'

alice = web3.personal.newAccount(password)
bob = web3.personal.newAccount(password)
web3.personal.unlockAccount(alice, password, 60)
tx = {from: alice, to: bob, value: 1000000000000000000, gas: 21000, gasPrice: 10000000000 }
web3.eth.sendTransaction(tx)